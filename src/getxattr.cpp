#include <stream9/linux/xattr/setxattr.hpp>

#include <string>

#include <sys/xattr.h>

#include <stream9/null.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::linux {

opt<std::string>
getxattr(cstring_ptr const& path,
         cstring_ptr const& name)
{
    try {
        using len_t = stream9::safe_integer<ssize_t, -1>;

        opt<std::string> result;
        result.emplace();

        auto n_try = 0;
        while (true) {
            len_t const n2 = ::getxattr(path, name, nullptr, 0);
            if (n2 == -1) {
                if (errno == ENODATA || errno == ERANGE) { // undocumented behavior but we get ERANGE when name is ""
                    result = null;
                    return result;
                }
                else {
                    throw error {
                        "getxattr()",
                        lx::make_error_code(errno),
                    };
                }
            }

            result->resize(n2);

            len_t const n1 =
                ::getxattr(path, name, result->data(), result->size());
            if (n1 == -1) {
                if (errno == ENODATA) {
                    result = null;
                    return result;
                }
                if (errno == ERANGE && n_try < 10) {
                    ++n_try;
                }
                else {
                    throw error {
                        "getxattr()",
                        lx::make_error_code(errno),
                    };
                }
            }
            else {
                if (n1 > 0 && result->back() == '\0') {
                    result->resize(n1 - 1);
                }
                else {
                    result->resize(n1);
                }

                break;
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error({
            { "path", path },
            { "name", name },
        });
    }
}

opt<std::string>
lgetxattr(cstring_ptr const& path,
          cstring_ptr const& name)
{
    try {
        using len_t = stream9::safe_integer<ssize_t, -1>;

        opt<std::string> result;
        result.emplace();

        for (auto n_try = 0; n_try < 10; ++n_try) {
            len_t const n2 = ::lgetxattr(path, name, nullptr, 0);
            if (n2 == -1) {
                if (errno == ENODATA) {
                    result = null;
                    return result;
                }
                else {
                    throw error {
                        "lgetxattr()",
                        lx::make_error_code(errno),
                    };
                }
            }
            result->resize(n2);

            len_t const n1 =
                ::lgetxattr(path, name, result->data(), result->size());
            if (n1 == -1) {
                if (errno == ENODATA) {
                    result = null;
                    return result;
                }
                if (errno != ERANGE) {
                    throw error {
                        "lgetxattr()",
                        lx::make_error_code(errno),
                    };
                }
            }
            else {
                if (n1 > 0 && result->back() == '\0') {
                    result->resize(n1 - 1);
                }
                else {
                    result->resize(n1);
                }

                break;
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error({
            { "path", path },
            { "name", name },
        });
    }
}

} // namespace stream9::linux
