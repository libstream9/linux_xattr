#include <stream9/linux/xattr/setxattr.hpp>

#include <string>

#include <sys/xattr.h>

#include <stream9/safe_integer.hpp>

namespace stream9::linux {

static json::string
flags_to_symbol(int const flags)
{
    json::string s;

    if (flags == XATTR_CREATE) {
        s = "XATTR_CREATE";
    }
    else if (flags == XATTR_REPLACE) {
        s = "XATTR_REPLACE";
    }
    else {
        s = "unknown flag: ";
        s.append(std::to_string(flags));
    }

    return s;
}

void
setxattr(cstring_ptr const& path,
         cstring_ptr const& name,
         std::string_view const value,
         int const flags)
{
    auto const rc = ::setxattr(path, name, value.data(), value.size(), flags);
    if (rc == -1) {
        json::object cxt {
            { "path", path },
            { "name", name },
            { "value", value },
        };

        if (flags) {
            cxt["flags"] = flags_to_symbol(flags);
        }

        throw error {
            "setxattr()",
            lx::make_error_code(errno),
            cxt
        };
    }
}

void
lsetxattr(cstring_ptr const& path,
          cstring_ptr const& name,
          std::string_view const value,
          int const flags)
{
    auto const rc = ::lsetxattr(path, name, value.data(), value.size(), flags);
    if (rc == -1) {
        json::object cxt {
            { "path", path },
            { "name", name },
            { "value", value },
        };

        if (flags) {
            cxt["flags"] = flags_to_symbol(flags);
        }

        throw error {
            "lsetxattr()",
            lx::make_error_code(errno),
            cxt
        };
    }
}

} // namespace stream9::linux
