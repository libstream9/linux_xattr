#include <stream9/linux/xattr/listxattr.hpp>

#include <sys/xattr.h>

#include <stream9/array.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::linux {

template<typename T, typename ErrorFn>
T
do_listxattr(cstring_ptr const& path, ErrorFn fn)
{
    using len_t = stream9::safe_integer<ssize_t, -1>;

    array<char> buf;

    auto n_try = 0;
    while (true) {
        len_t const buf_len = ::listxattr(path, nullptr, 0);
        if (buf_len == -1) {
            return fn(errno, std::source_location::current());
        }

        buf.resize(buf_len);

        len_t const len = ::listxattr(path, buf.data(), buf.size());
        if (len == -1) {
            if (errno == ERANGE && n_try < 10) {
                ++n_try;
            }
            else {
                return fn(errno, std::source_location::current());
            }
        }
        else {
            break;
        }
    }

    return { std::move(buf) };
}

xattr_name_array
listxattr(cstring_ptr const& path)
{
    try {
        return do_listxattr<xattr_name_array>(path,
            [&](int e, auto loc) -> xattr_name_array {
                throw error {
                    "listxattr()",
                    lx::make_error_code(e),
                    std::move(loc)
                };
            });
    }
    catch (...) {
        rethrow_error({
            { "path", path },
        });
    }
}

void
tag_invoke(json::value_from_tag,
           json::value& v,
           xattr_name_array const& names)
{
    auto& arr = v.emplace_array();
    for (auto const& name: names) {
        arr.push_back(name);
    }
}

} // namespace stream9::linux

namespace stream9::linux::nothrow {

outcome<xattr_name_array, errc>
listxattr(cstring_ptr const& path) noexcept
{
    using R = outcome<xattr_name_array, errc>;

    return do_listxattr<R>(path,
        [&](int e, auto) -> R {
            return { st9::error_tag(), static_cast<errc>(e) };
        });
}

} // namespace stream9::linux::nothrow
