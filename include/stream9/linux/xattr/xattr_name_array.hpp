#ifndef STREAM9_LINUX_XATTR_XATTR_NAME_ARRAY_HPP
#define STREAM9_LINUX_XATTR_XATTR_NAME_ARRAY_HPP

#include "xattr_name_iterator.hpp"

#include <stream9/array.hpp>

namespace stream9::linux {

class xattr_name_array
{
public:
    xattr_name_array(array<char> buf) noexcept
        : m_buf { std::move(buf) } {}

    xattr_name_iterator begin() const noexcept
    {
        return std::span<char const>(m_buf);
    }

    std::default_sentinel_t end() const noexcept
    {
        return {};
    }

private:
    array<char> m_buf;
};

void tag_invoke(json::value_from_tag, json::value&, xattr_name_array const&);

} // namespace stream9::linux

#endif // STREAM9_LINUX_XATTR_XATTR_NAME_ARRAY_HPP
