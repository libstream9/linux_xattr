#ifndef STREAM9_LINUX_XATTR_SETXATTR_HPP
#define STREAM9_LINUX_XATTR_SETXATTR_HPP

#include <string_view>

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/namespace.hpp>

namespace stream9::linux {

void
setxattr(cstring_ptr const& path,
         cstring_ptr const& name,
         std::string_view value,
         int flags = 0);

void
lsetxattr(cstring_ptr const& path,
          cstring_ptr const& name,
          std::string_view value,
          int flags = 0);

} // namespace stream9::linux

#endif // STREAM9_LINUX_XATTR_SETXATTR_HPP
