#ifndef STREAM9_LINUX_XATTR_LISTXATTR_HPP
#define STREAM9_LINUX_XATTR_LISTXATTR_HPP

#include "xattr_name_array.hpp"

#include <stream9/linux/error.hpp>
#include <stream9/linux/namespace.hpp>

#include <stream9/cstring_ptr.hpp>
#include <stream9/outcome.hpp>

namespace stream9::linux {

xattr_name_array
listxattr(cstring_ptr const& path);

namespace nothrow {

outcome<xattr_name_array, errc>
listxattr(cstring_ptr const& path) noexcept;

} // namespace nothrow

} // namespace stream9::linux

#endif // STREAM9_LINUX_XATTR_LISTXATTR_HPP
