#ifndef STREAM9_LINUX_XATTR_XATTR_NAME_ITERATOR_HPP
#define STREAM9_LINUX_XATTR_XATTR_NAME_ITERATOR_HPP

#include <cstddef>
#include <iterator>
#include <span>

#include <stream9/linux/namespace.hpp>

#include <stream9/iterators.hpp>
#include <stream9/strings/query/size.hpp>
#include <stream9/cstring_view.hpp>

namespace stream9::linux {

class xattr_name_iterator : public stream9::iterators::iterator_facade<
                                      xattr_name_iterator,
                                      std::input_iterator_tag,
                                      cstring_view >
{
public:
    xattr_name_iterator(std::span<char const> const buf) noexcept
    {
        if (!buf.empty()) {
            m_cur = buf.data();
            m_len = str::size(m_cur);
            m_last = buf.data() + buf.size();
        }
        else {
            m_cur = buf.data();
            m_len = 0;
            m_last = buf.data();
        }
    }

    xattr_name_iterator() = default; // partially-formed

private:
    friend class stream9::iterators::iterator_core_access;

    reference dereference() const noexcept
    {
        return { m_cur, m_len };
    }

    void increment() noexcept
    {
        m_cur += m_len + 1;

        if (m_cur != m_last) {
            m_len = str::size(m_cur);
        }
    }

    bool equal(xattr_name_iterator const& other) const noexcept
    {
        return m_cur == other.m_cur;
    }

    bool equal(std::default_sentinel_t) const noexcept
    {
        return m_cur == m_last;
    }

private:
    char const* m_cur;
    std::size_t m_len;
    char const* m_last;
};

} // namespace stream9::linux

#endif // STREAM9_LINUX_XATTR_XATTR_NAME_ITERATOR_HPP
