#ifndef STREAM9_LINUX_XATTR_GETXATTR_HPP
#define STREAM9_LINUX_XATTR_GETXATTR_HPP

#include <string>

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/namespace.hpp>
#include <stream9/optional.hpp>

namespace stream9::linux {

opt<std::string>
getxattr(cstring_ptr const& path,
         cstring_ptr const& name);

opt<std::string>
lgetxattr(cstring_ptr const& path,
          cstring_ptr const& name);

} // namespace stream9::linux

#endif // STREAM9_LINUX_XATTR_GETXATTR_HPP
